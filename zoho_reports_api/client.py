import requests


class ZohoException(Exception):
    pass


class Client:
    def __init__(self, token):
        self.auth_token = token

    def add_row(self, table_uri, values):
        body = {
            "ZOHO_ACTION": "ADDROW",
            "ZOHO_OUTPUT_FORMAT": "JSON",
            "ZOHO_ERROR_FORMAT": "JSON",
            "ZOHO_API_VERSION": "1.0",
            "authtoken": self.auth_token
        }

        body.update(**values)

        resp = requests.post(table_uri, data=body)

        try:
            resp.raise_for_status()
        except requests.HTTPError as e:
            raise ZohoException(e)

        resp = resp.json()

        if "error" in resp["response"]:
            message = resp["response"]["error"].get("message", "Unknown error")
            raise ZohoException(message)

        return resp

    def update(self, table_uri, values, criteria=None):
        body = {
            "ZOHO_ACTION": "UPDATE",
            "ZOHO_OUTPUT_FORMAT": "JSON",
            "ZOHO_ERROR_FORMAT": "JSON",
            "ZOHO_API_VERSION": "1.0",
            "authtoken": self.auth_token
        }

        if criteria:
            body["ZOHO_CRITERIA"] = criteria

        body.update(**values)

        resp = requests.post(table_uri, data=body)

        try:
            resp.raise_for_status()
        except requests.HTTPError as e:
            raise ZohoException(e)

        resp = resp.json()

        if "error" in resp["response"]:
            message = resp["response"]["error"].get("message", "Unknown error")
            raise ZohoException(message)

        return resp["response"]["result"].get("updatedRows", "0")

    def import_data(self, table_uri, data):
        body = {
            "ZOHO_ACTION": "IMPORT",
            "ZOHO_OUTPUT_FORMAT": "JSON",
            "ZOHO_ERROR_FORMAT": "JSON",
            "ZOHO_API_VERSION": "1.0",
            "authtoken": self.auth_token,
            "ZOHO_IMPORT_TYPE": "APPEND",
            "ZOHO_IMPORT_FILETYPE": "JSON",
            "ZOHO_IMPORT_DATA": data,
            "ZOHO_ON_IMPORT_ERROR": "SKIPROW",
            "ZOHO_AUTO_IDENTIFY": "TRUE",
            "ZOHO_CREATE_TABLE": "FALSE"
        }

        resp = requests.post(table_uri, data=body)

        try:
            resp.raise_for_status()
        except requests.HTTPError as e:
            raise ZohoException(e)

        resp = resp.json()

        if "error" in resp["response"]:
            message = resp["response"]["error"].get("message", "Unknown error")
            raise ZohoException(message)

        return resp["response"]["result"].get("importSummary")

    def delete(self, table_uri, criteria=None):
        body = {
            "ZOHO_ACTION": "DELETE",
            "ZOHO_OUTPUT_FORMAT": "JSON",
            "ZOHO_ERROR_FORMAT": "JSON",
            "ZOHO_API_VERSION": "1.0",
            "authtoken": self.auth_token
        }

        if criteria:
            body["ZOHO_CRITERIA"] = criteria

        resp = requests.post(table_uri, data=body)

        try:
            resp.raise_for_status()
        except requests.HTTPError as e:
            raise ZohoException(e)

        resp = resp.json()

        if "error" in resp["response"]:
            message = resp["response"]["error"].get("message", "Unknown error")
            raise ZohoException(message)

        return resp["response"]["result"].get("deletedrows", "0")
