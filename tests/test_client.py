from unittest import TestCase
from unittest.mock import patch, MagicMock

from zoho_reports_api.client import Client, ZohoException


class ClientTestCase(TestCase):
    def setUp(self):
        self.client = Client("TOKEN")
        self.table_uri = "https://reportsapi.zoho.com/api/user@example.com/Workspace/Table"

    @patch("zoho_reports_api.client.requests.post")
    def test_add_row(self, mocked_post):
        result = self.client.add_row(self.table_uri, {"column": "value"})

        body = {
            "ZOHO_ACTION": "ADDROW",
            "ZOHO_OUTPUT_FORMAT": "JSON",
            "ZOHO_ERROR_FORMAT": "JSON",
            "ZOHO_API_VERSION": "1.0",
            "authtoken": "TOKEN",
            "column": "value"
        }

        mocked_post.assert_called_once_with(self.table_uri, data=body)

    @patch("zoho_reports_api.client.requests.post")
    def test_add_row_with_error(self, mocked_post):
        resp = {
            'response': {
                'uri': 'api/user@example.com/Workspace/Table',
                'action': 'ADDROW',
                'error': {
                    'code': 1,
                    'message': 'message'
                }
            }
        }


        mocked_post.return_value.json.return_value = resp

        with self.assertRaises(ZohoException):
            self.client.add_row(self.table_uri, {"column": "value"})

    @patch("zoho_reports_api.client.requests.post")
    def test_update(self, mocked_post):
        result = self.client.update(self.table_uri, {"column": "value"})

        body = {
            "ZOHO_ACTION": "UPDATE",
            "ZOHO_OUTPUT_FORMAT": "JSON",
            "ZOHO_ERROR_FORMAT": "JSON",
            "ZOHO_API_VERSION": "1.0",
            "authtoken": "TOKEN",
            "column": "value"
        }

        mocked_post.assert_called_once_with(self.table_uri, data=body)

    @patch("zoho_reports_api.client.requests.post")
    def test_update_with_criteria(self, mocked_post):
        result = self.client.update(self.table_uri, {"column": "value"}, "column = value")

        body = {
            "ZOHO_ACTION": "UPDATE",
            "ZOHO_OUTPUT_FORMAT": "JSON",
            "ZOHO_ERROR_FORMAT": "JSON",
            "ZOHO_API_VERSION": "1.0",
            "ZOHO_CRITERIA": "column = value",
            "authtoken": "TOKEN",
            "column": "value"
        }

        mocked_post.assert_called_once_with(self.table_uri, data=body)

    @patch("zoho_reports_api.client.requests.post")
    def test_update_with_error(self, mocked_post):
        resp = {
            'response': {
                'uri': 'api/user@example.com/Workspace/Table',
                'action': 'ADDROW',
                'error': {
                    'code': 1,
                    'message': 'message'
                }
            }
        }

        mocked_post.return_value.json.return_value = resp

        with self.assertRaises(ZohoException):
            self.client.update(self.table_uri, {"column": "value"})

    @patch("zoho_reports_api.client.requests.post")
    def test_import(self, mocked_post):
        resp = {
            "response": {
                'uri': 'api/user@example.com/Workspace/Table',
                "action": "IMPORT",
                "result": {
                    "importSummary": {
                        "totalColumnCount": 3,
                        "selectedColumnCount": 3,
                        "totalRowCount": 50,
                        "successRowCount": 48,
                        "warnings": 0,
                        "importOperation": "created",
                        "importType": "APPEND"
                    },
                    "columnDetails": {
                        "Name": "Plain Text",
                        "Date Of Birth": "Date",
                        "Salary": "Number"
                    },
                    "importErrors": ""
                }
            }
        }

        mocked_post.return_value.json.return_value = resp

        result = self.client.import_data(self.table_uri, '{"column": "value"}')

        body = {
            "ZOHO_ACTION": "IMPORT",
            "ZOHO_OUTPUT_FORMAT": "JSON",
            "ZOHO_ERROR_FORMAT": "JSON",
            "ZOHO_API_VERSION": "1.0",
            "authtoken": "TOKEN",
            "ZOHO_IMPORT_TYPE": "APPEND",
            "ZOHO_IMPORT_FILETYPE": "JSON",
            "ZOHO_IMPORT_DATA": '{"column": "value"}',
            "ZOHO_ON_IMPORT_ERROR": "SKIPROW",
            "ZOHO_AUTO_IDENTIFY": "TRUE",
            "ZOHO_CREATE_TABLE": "FALSE"
        }

        mocked_post.assert_called_once_with(self.table_uri, data=body)

        self.assertEqual(result, resp["response"]["result"]["importSummary"])

    @patch("zoho_reports_api.client.requests.post")
    def test_import_with_error(self, mocked_post):
        resp = {
            'response': {
                'uri': 'api/user@example.com/Workspace/Table',
                'action': 'IMPORT',
                'error': {
                    'code': 1,
                    'message': 'message'
                }
            }
        }

        mocked_post.return_value.json.return_value = resp

        with self.assertRaises(ZohoException):
            self.client.import_data(self.table_uri, '{"column": "value"}')

    @patch("zoho_reports_api.client.requests.post")
    def test_delete(self, mocked_post):
        resp = {
            "response": {
                'uri': 'api/user@example.com/Workspace/Table',
                "action": "DELETE",
                "result": {
                    "message": "Deleted rows",
                    "deletedrows":"4"
                }
            }
        }

        mocked_post.return_value.json.return_value = resp

        result = self.client.delete(self.table_uri)

        body = {
            "ZOHO_ACTION": "DELETE",
            "ZOHO_OUTPUT_FORMAT": "JSON",
            "ZOHO_ERROR_FORMAT": "JSON",
            "ZOHO_API_VERSION": "1.0",
            "authtoken": "TOKEN"
        }

        mocked_post.assert_called_once_with(self.table_uri, data=body)

        self.assertEqual(result, resp["response"]["result"]["deletedrows"])

    @patch("zoho_reports_api.client.requests.post")
    def test_delete_with_criteria(self, mocked_post):
        resp = {
            "response": {
                'uri': 'api/user@example.com/Workspace/Table',
                "action": "DELETE",
                "criteria": "\"column\" = \'value\'",
                "result": {
                    "message": "Deleted rows",
                    "deletedrows":"4"
                }
            }
        }

        mocked_post.return_value.json.return_value = resp

        result = self.client.delete(self.table_uri, "column = value")

        body = {
            "ZOHO_ACTION": "DELETE",
            "ZOHO_OUTPUT_FORMAT": "JSON",
            "ZOHO_ERROR_FORMAT": "JSON",
            "ZOHO_API_VERSION": "1.0",
            "ZOHO_CRITERIA": "column = value",
            "authtoken": "TOKEN"
        }

        mocked_post.assert_called_once_with(self.table_uri, data=body)

        self.assertEqual(result, resp["response"]["result"]["deletedrows"])

    @patch("zoho_reports_api.client.requests.post")
    def test_delete_with_error(self, mocked_post):
        resp = {
            'response': {
                'uri': 'api/user@example.com/Workspace/Table',
                'action': 'DELETE',
                'error': {
                    'code': 1,
                    'message': 'message'
                }
            }
        }

        mocked_post.return_value.json.return_value = resp

        with self.assertRaises(ZohoException):
            self.client.delete(self.table_uri)

