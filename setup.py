from setuptools import setup, find_packages


setup(
    name='zoho_reports_api',
    version='1.0.0',
    description='Zoho Reports API',
    author='Artistanbul Developers',
    author_email='devs@artistanbul.io',
    url='https://gitlab.com/artistanbul/idata/zoho-reports-api',
    install_requires=[
        "requests"
    ],
    packages=find_packages(exclude=('tests', 'docs')),
    test_suite="tests"
)
